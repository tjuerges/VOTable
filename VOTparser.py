#!/usr/bin/env python

import sys
import VOThandler
from optparse import OptionParser
import rlcompleter
import readline

readline.parse_and_bind("tab: complete")

__HELP__="""
Parser for VOTables created by the ALMA DeviceMonitor component.

SYNOPSIS: python -i VOTparser.py -f <VOTable.xml> [-r <ResourceName>] [-c <contentID>] [-b <beginTime>] [-e <endTime>]


<VOTable.xml>: is the name of an ALMA VOTable file created by the
DeviceMonitor component.

<ResourceName>: optional parameter to select one resource according to the name
or ID attribute.

<beginTime>: defines the begin of a timespan in which the monitor point data was
collected. Format: ISO-8601, YYYY-MM-DDTHH:mm:SS as in 2005-10-05T16:41:22

<endTime>: defines the end of a timespan in which the monitor point data was
collected. Format: ISO-8601, YYYY-MM-DDTHH:mm:SS as in 2005-10-05T16:41:22

HISTORY: Thomas Juerges [NRAO, Oct. 2005]
		 0.1, 2005-10-05: First try.
"""

votable = VOThandler.VOThandler()

def usage():
	print __HELP__

def main(args):
	"""
	"""

	optParser = OptionParser()
	optParser.add_option("-f", "--file", dest="filename",
										 help="Load data from FILENAME. This option is mandatory!", metavar="FILENAME")
	optParser.add_option("-r", "--resource", dest="resource",
										 help="Just print the data for a specific RESOURCE.", metavar="RESOURCE")
	optParser.add_option("-b", "--begin", dest="begin",
										 help="Print out data which has been collected after this date.", metavar="ISO-8601-DATE")
	optParser.add_option("-e", "--end", dest="end",
										 help="Print out data which has been collected before this date.", metavar="ISO-8601-DATE")

	(options, args) = optParser.parse_args()

	if options.filename == None:
		usage()
		sys.exit()

	if options.resource != None:
		votable.setResourceName(options.resource)
		print "Using Resource ", votable.resourceName

	if options.begin != None:
		votable.setStartTime(options.begin)
		print "Start time: ", votable.printTimes("start")

	if options.end != None:
		votable.setEndTime(options.end)
		print "End time: ", votable.printTimes("end")

	if options.filename != None:
		votable.setFile(options.filename)
		print "Using filename ", votable.fileName

	votable.getData()

	print "\n\nThe VOTable data and everything related is in the variable votable."
	print "Try votable.[TAB] to see the available methods and vriables."

if __name__ == '__main__':
	main(sys.argv)
