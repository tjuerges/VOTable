import os
import sys
import time
import datetime
import MultipartHandler
import rlcompleter
import readline

readline.parse_and_bind("tab: complete")

class VOThandler:

	"""	Parser for VOTables created by the ALMA DeviceMonitor component.
	HISTORY: Thomas Juerges [NRAO, Oct. 2005]
	0.1, 2005-10-06: First try.
	"""


	acsdata_path = ""
	fileName = ""
	resourceName = ""
	_file = ""
	mimeParser = ""
	message = ""
	res = ""
	cidI = ""
	cids = ""
	binParts = ""
	binarr = ""
	monitorPoints = {}
	iso8601Format = "%Y-%m-%dT%H:%M:%S"
	_2037 = datetime.datetime(2037, 12, 31, 23, 59, 59)
	_1980 = datetime.datetime(1980, 1, 1, 0, 0, 0, 0)
	_1970 = datetime.datetime(1970, 1, 1, 0, 0, 0, 0)
	_19701980Offset = _1980 - _1970
	timeBegin = _1970
	timeEnd = _2037
	deltaT = ""

	def convertTime(self, string=None):
		"""Converts a time in "YYYY-MM-DDTHH:MM:SS" format to a struct_time.
		"""
		if string == None:
			print "Please pass a valid string!"
			return ""
		else:
			return time.strptime(string,"%Y-%m-%dT%H:%M:%S")

	def timeToFloat(self, t=None):
		"""Converts a struct_time to a float.
		"""
		if t == None:
			print "Please pass a valid struct_time!"
			return ""
		else:
			return time.mktime(self.convertTime(t))

	def makeDatetime(self, t=None):
		"""Constructs a datetime object from a float timestamp.
		"""
		if t == None:
			print "Please pass a valid float timestamp!"
		else:
			return datetime.datetime.fromtimestamp(self.timeToFloat(t))

	def clearTime(self, which=None):
		"""Sets either the start time to 1970-01-01T00:00:00 (parameter =
		"start | begin") or the end time to 2037-12-31T23:59:59 (parameter =
		"end").
		"""
		if which == None:
			print "Please pass either [start | begin] or end!"
			return
		if (which == "start") or (which == "begin"):
			self.timeBegin = self._1970
		elif which == "end":
			self.timeEnd = self._2037
		else:
			print "Either select start or begin for the start time or"
			print "select end for the end time."
		if (self.timeBegin != "") and (self.timeEnd != ""):
			self.deltaT = self.timeEnd - self.timeBegin

	def clearStartTime(self):
		"""Sets the start time to 1970-01-01T00:00:00.
		This is done by calling self.clearTime("start").
		"""
		self.clearTime("begin")

	def clearEndTime(self):
		"""Sets the end time to 2037-12-31T23:59:59.
		This is done by calling self.clearTime("end").
		"""
		self.clearTime("end")

	def _clear(self):
		"""Clears all data but the preselected times.
		"""
		self.fileName = ""
		self.resourceName = ""
		self.cid = ""
		self._file = ""
		self.mimeParser = ""
		self.message = ""
		self.res = ""
		self.cidI = ""
		self.cids = ""
		self.binParts = ""
		self.binarr = ""
		self.monitorPoints = {}

	def _openFile(self):
		"""For internal use only!
		Open the preselected filename self.fileName.
		"""
		if self._file != "":
			name = self._file.name
		else:
			name = ""
		if len(name) != 0:
			print "File is already open. Current file is ", name
			print "Close the file first with unloadFile()!"
		else:
			try:
				self._file = open(self.fileName)
			except:
				print "Unable to open ", self.fileName

	def _closeFile(self):
		"""For internal use only!
		Close the preselected file self._file.name.
		"""
		name = self._file.name
		if len(name) == 0:
			print "No file has been loaded. Load one first with loadFile(FILENAME) or"
			print "setFile(FILENAME)!"
		else:
			self._file.close()
			self._file = ""

	def usage(self):
		"""Prints a help message.
		"""
		print "Say help(VOThandler)!"

	def loadFile(self, filename=None):
		"""Load and parse the MIME parts of the ALMA VOTable file "filename".
No data interpretation is done here, call self.getData() afterwards.
"""
		if filename == None:
			print "Please pass a valid file name!"
			return

		success = True
		name = ""

		if os.path.exists(self.acsdata_path+"/tmp/"+filename):
			name = self.acsdata_path+"/tmp/"+filename
		elif os.path.exists(self.acsdata_path+"/"+filename):
			name = self.acsdata_path+"/"+filename
		elif os.path.exists("./"+filename):
			name = "./"+filename
		else:
			name = filename

		if os.path.exists(name):
			self.fileName = name
			print "\nFile %s has been selected.\n" % self.fileName

			self._openFile()
			print "File has been opened, start parsing data..."

			self.mimeParser = MultipartHandler.Parser()  # instantiate the email Parser

			try:
				self.message = self.mimeParser.parse(self._file) # and parse the file
				print "Parsing ok."
			except Exception, e:
				print "Parsing failed: " + str(e)
				success = False
			self._closeFile()
			print "File closed.\n\n"

		else:
			print "The file %s does not exist. Please correct any typos and try again!" %name

	def setFile(self, name=None):
		"""Does the same as loadFile(filename). See there for a description.
"""
		if name == None:
			print "Please pass a file name!"
			return

		self.loadFile(name)

	def unloadFile(self):
		"""Make sure that the before loaded file is really closed to prevent it
from data import loss.
"""
		self._closeFile()

	def clearData(self):
		"""Clears all data but the preselected times.
		"""
		self._clear()

	def clearAll(self):
		"""Clears all data AND the start and end times.
		"""
		self._clear()
		self.clearStartTime()
		self.clearEndTime()

	def setResource(self, name=None):
		"""Set the preselected resource name to "name".
		"""
		if name == None:
			print "Please pass a resource name!"
			return

		self.resourceName = name

	def setTime(self, which="start", time=None):
		"""Set either the start or the end time to "time". For the start time,
		which = "start" | "begin", for the end time, which = "end".
		time has to be in ISO-8601 format (YYY-MM-DDTHH:MM:SS).
		"""
		if time == None:
				print "Eh, the passed time parameter was empty!"
				return

		if (which == "start") or (which == "begin"):
			self.timeBegin = self.makeDatetime(time)
		elif which == "end":
			self.timeEnd = self.makeDatetime(time)
		else:
			print "Either select \"start\" or \"begin\" for the start time or"
			print "select \"end\" for the end time."

	def setStartTime(self, time=None):
		"""Set the start time to time.
		time has to be in ISO-8601 format (YYY-MM-DDTHH:MM:SS).
		"""
		self.setTime("begin", time)

	def setEndTime(self, time=None):
		"""Set the end time to time.
		time has to be in ISO-8601 format (YYY-MM-DDTHH:MM:SS).
		"""
		self.setTime("end", time)

	def printTimes(self,which="begin"):
		"""Print either the start or the end time depending on which. Possible
		values are for the start time "start" or "begin", for the end time
		"end" only.
		"""
		if (which == "begin") or (which == "start"):
			print "Start time is ", self.timeBegin
		elif which == "end":
			print "End time is ", self.timeEnd
		else:
			print "Either select start or begin for the start time or"
			print "select end for the end time."

	def printDeltaT(self):
		"""Print out thew time difference in seconds between end time and start
		time.
		"""
		foo = float(self.deltaT.seconds) + float(self.deltaT.microseconds) / 1000000.0
		print "The time span is %d days, %f seconds" % (self.deltaT.days, foo)

	def time(self,which="begin"):
		"""Return either the start or the end time as float value. Set which to
		"start" or "begin" for the start time or set it to "end" for the end
		time.
		"""
		if (which == "begin") or (which == "start"):
			return self.timeToFloat(self.timeBegin)
		elif which == "end":
			return self.timeToFloat(self.timeEnd)
		else:
			print "Either select start or begin for the start time or"
			print "select end for the end time."
			return ""

	def getData(self):
		"""Do the real distillation of data from the MIME-VOTable mixture.
		Call this method after loading a new file or new data.
		"""
		# if cid is specified just return that part and bail out
		if len(self.cid) > 0:
			self.part = MultipartHandler.getPartId(self.message, self.cid)
			return part

		# get the first xml part of the email...
		xmlParts = MultipartHandler.getMimeParts(self.message, 'text/xml')
		if len(xmlParts) == 0:
			print "No text/xml part found!"
			return ''
		else:
			xml = xmlParts[0].get_payload()  # There should only be one!

		# ...and parse it
		try:
			root = MultipartHandler.VOTable.parseString(xml)
		except Exception, e:
			print "XML parsing failed! " + str(e)
			return ''

		(self.res, self.cidI, self.cids) = MultipartHandler.interpretVotable(root, selection=self.resourceName)

		try:
			(self.binParts, self.binarr) = MultipartHandler.interpretBinary(self.message, self.cids, self.cidI)
		except Exception, e:
			print "Interpretation of binary attachment failed: " + str(e)
			return ''

		for i in range(0, len(self.binarr), 2):
			raw_name = str(self.cids[i+1])

			[savedAt_asString, name] = raw_name.split(".")[:2]

			try:
				savedAt_asFloat = float(savedAt_asString)
			except:
				savedAt_asFloat = float(self.timeToFloat(savedAt_asString))
#			savedAt = time.ctime(savedAt_asFloat)
			savedAt = datetime.datetime.fromtimestamp(savedAt_asFloat)

			itsType = str(self.cidI[raw_name][0])
			print "Collecting data for %s of type %s..." % (name, itsType)

			self.monitorPoints[str(name)] = []
			seq = None
			try:
				if len(self.binarr[i]) != len(self.binarr[i+1]):
					seq = 1
			except:
				None
# It is not a sequence,
			if seq == None:
				for j in range(len(self.binarr[i])):
					offset = datetime.timedelta(0,self.binarr[i][j])
					timeOfEvent = self._1970 + self._19701980Offset + offset
					bar = (itsType, savedAt, timeOfEvent, self.binarr[i+1][j])
					self.monitorPoints[str(name)].append(bar)
			else:
				seqLen = len(self.binarr[i+1]) / len(self.binarr[i])
				for j in range(len(self.binarr[i])):
					val = []
					timestamp = []
					for k in range(seqLen):
						val.append(self.binarr[i+1][j+k])
						offset = datetime.timedelta(0,self.binarr[i][j])
						timeOfEvent = self._1970 + self._19701980Offset + offset
						timestamp.append(timeOfEvent)
					bar = (itsType, savedAt, timestamp, val)
					self.monitorPoints[str(name)].append(bar)
		print "Ready.\n"

	def getDataFromFile(self,filename=None):
		"""Do the real distillation of data from the MIME-VOTable mixture.
		Call this method after loading a new file or new data.
		You can see, this method takes only one mopre parameter. This is for
		the extreme lazy people who want everything in one go. Pass the
		filename of the data file and everything is ready afterwards. Just
		do a self.list() to print all the relevant monitor data.
		"""
		if filename == None:
			print "Please pass a valid file name!"
			return

		self.clearData()
		self.loadFile(filename)
		self.getData()

	def listProperties(self):
		"""List the available properties for which data can be provided."""
		for k in self.monitorPoints.keys():
			print k

	def listEntry(self, key=None, start=None, end=None):
		"""Print the monitor data of property "key" in a human readable format. To
find out which keys are possible, try listEntry()!
	"""
		if key == None:
			print "Please specify a property name!"
			return
		if start == None:
			start = self.timeBegin
		if end == None:
			end = self.timeEnd

		print 80 * "*"
		print "Name: %s" % key
		print "Archived on: %s" % str(self.monitorPoints[key][0][1])
		print 80 * "-"
		for i in range(len(self.monitorPoints[key])):
			seq = None
			try:
				if len(self.monitorPoints[key][i][3]) != 1:
					seq = 1
			except:
				None
# It is not a sequence,
			if seq == None:
				timestamp = self.monitorPoints[key][i][2]
				if (start < timestamp) and (timestamp < end):
					print "Timestamp: %s\tValue = " % str(timestamp.isoformat()),
					print self.monitorPoints[key][i][3]
			else:
				for j in range(len(self.monitorPoints[key][i][3])):
					timestamp = self.monitorPoints[key][i][2][j]
					if (start < timestamp) and (timestamp < end):
						print "Timestamp: %s\tValue = " % str(timestamp.isoformat()),
						print str(self.monitorPoints[key][i][3][j])
		print "\n"

	def list(self):
		"""Print the monitor data of every property in a human readable format.
		"""
		for k in self.monitorPoints.keys():
			self.listEntry(k)

	def __init__(self):
		"""Initialise the VOTable class.
		"""
		self.clearAll()
		if os.environ.has_key("ACSDATA"):
			self.acsdata_path = os.environ.get("ACSDATA")
