#!/usr/bin/env python
#
#  ALMA - Atacama Large Millimiter Array
#  (c) European Southern Observatory, 2002
#  Copyright by ESO (in the framework of the ALMA collaboration),
#  All rights reserved
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
#  MA 02111-1307  USA
#


#
# Generated Mon Sep  1 11:48:55 2003 by generateDS.py.
#
# Modified by A.Wicenec [ESO]
#

import sys,StringIO
import getopt
import re
from xml.dom import minidom
from xml.dom import Node
#import yaml

#
# If you have installed IPython you can uncomment and use the following.
# IPython is available from http://www-hep.colorado.edu/~fperez/ipython.
#
#from IPython.Shell import IPythonShellEmbed
#IPShell = IPythonShellEmbed('-nosep',
#    banner = 'Entering interpreter.  Ctrl-D to exit.',\
#    exit_msg = 'Leaving Interpreter.')

# Use the following line where and when you want to drop into the
# IPython shell:
#    IPShell(vars(), '<a msg>')

# Use the following line where and when you want to drop into the
# IPython shell:
#    IPShell()
#
# Support/utility functions.
#

def showIndent(outfile, level):
	for idx in range(level):
		outfile.write('    ')

def quote_xml(inStr):
	s1 = inStr
	s1 = s1.replace('&', '&amp;')
	s1 = s1.replace('<', '&lt;')
	return s1


#
# Data representation classes.
#

class VOTABLE:
	subclass = None
	def __init__(self, DESCRIPTION=None, DEFINITIONS=None, INFO=None, RESOURCE=None, ID=None, version=''):
		self.DESCRIPTION = DESCRIPTION
		self.DEFINITIONS = DEFINITIONS
		if INFO is None:
			self.INFO = []
		else:
			self.INFO = INFO
		if RESOURCE is None:
			self.RESOURCE = []
		else:
			self.RESOURCE = RESOURCE
		self.ID = ID
		self.version = version
	def factory(*args):
		if VOTABLE.subclass:
			return apply(VOTABLE.subclass, args)
		else:
			return apply(VOTABLE, args)
	factory = staticmethod(factory)
	def getDescription(self): return self.DESCRIPTION
	def setDescription(self, DESCRIPTION): self.DESCRIPTION = DESCRIPTION
	def getDefinitions(self): return self.DEFINITIONS
	def setDefinitions(self, DEFINITIONS): self.DEFINITIONS = DEFINITIONS
	def getInfo(self): return self.INFO
	def addInfo(self, value): self.INFO.append(value)
	def setInfo(self, index, value): self.INFO[index] = value
	def getResource(self): return self.RESOURCE
	def addResource(self, value): self.RESOURCE.append(value)
	def setResource(self, index, value): self.RESOURCE[index] = value
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getVersion(self): return self.version
	def setVersion(self, version): self.version = version
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<VOTABLE xmlns:xsi='+\
					  '"http://www.w3.org/2001/XMLSchema-instance" '+\
					  'xsi:noNamespaceSchemaLocation='+\
					  '"http://vizier.u-strasbg.fr/xml/VOTable.xsd" ')
		outfile.write('ID="%s" version="%s">\n' % (self.ID, self.version, ))
		level += 1
		if self.DESCRIPTION:
			self.DESCRIPTION.export(outfile, level)
		if self.DEFINITIONS:
			self.DEFINITIONS.export(outfile, level)
		for INFO in self.INFO:
			INFO.export(outfile, level)
		for RESOURCE in self.RESOURCE:
			RESOURCE.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</VOTABLE>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs != None:
			if attrs.get('ID'):
				self.ID = attrs.get('ID').value
			if attrs.get('version'):
				self.version = attrs.get('version').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DESCRIPTION':
				if len(child.childNodes)>0:
					value = child.childNodes[0].nodeValue
				else:
					value = u''
				obj = DESCRIPTION.factory(value)
				obj.build(child)
				self.setDescription(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DEFINITIONS':
				obj = DEFINITIONS.factory()
				obj.build(child)
				self.setDefinitions(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'INFO':
				if len(child.childNodes)>0:
					text = child.childNodes[0].nodeValue
				else:
					text = u''
				obj = INFO.factory(text)
				obj.build(child)
				self.INFO.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'RESOURCE':
				obj = RESOURCE.factory()
				obj.build(child)
				self.RESOURCE.append(obj)
# end class VOTABLE


class RESOURCE:
	subclass = None
	def __init__(self, DESCRIPTION=None, INFO=None, COOSYS=None, PARAM=None, LINK=None, TABLE=None, RESOURCE=None, name=None, ID=None, type=''):
		self.DESCRIPTION = DESCRIPTION
		if INFO is None:
			self.INFO = []
		else:
			self.INFO = INFO
		if COOSYS is None:
			self.COOSYS = []
		else:
			self.COOSYS = COOSYS
		if PARAM is None:
			self.PARAM = []
		else:
			self.PARAM = PARAM
		if LINK is None:
			self.LINK = []
		else:
			self.LINK = LINK
		if TABLE is None:
			self.TABLE = []
		else:
			self.TABLE = TABLE
		if RESOURCE is None:
			self.RESOURCE = []
		else:
			self.RESOURCE = RESOURCE
		self.name = name
		self.ID = ID
		self.type = type
	def factory(*args):
		if RESOURCE.subclass:
			return apply(RESOURCE.subclass, args)
		else:
			return apply(RESOURCE, args)
	factory = staticmethod(factory)
	def getDescription(self): return self.DESCRIPTION
	def setDescription(self, DESCRIPTION): self.DESCRIPTION = DESCRIPTION
	def getInfo(self): return self.INFO
	def addInfo(self, value): self.INFO.append(value)
	def setInfo(self, index, value): self.INFO[index] = value
	def getCoosys(self): return self.COOSYS
	def addCoosys(self, value): self.COOSYS.append(value)
	def setCoosys(self, index, value): self.COOSYS[index] = value
	def getParam(self): return self.PARAM
	def addParam(self, value): self.PARAM.append(value)
	def setParam(self, index, value): self.PARAM[index] = value
	def getLink(self): return self.LINK
	def addLink(self, value): self.LINK.append(value)
	def setLink(self, index, value): self.LINK[index] = value
	def getTable(self): return self.TABLE
	def addTable(self, value): self.TABLE.append(value)
	def setTable(self, index, value): self.TABLE[index] = value
	def getResource(self): return self.RESOURCE
	def addResource(self, value): self.RESOURCE.append(value)
	def setResource(self, index, value): self.RESOURCE[index] = value
	def getName(self): return self.name
	def setName(self, name): self.name = name
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getType(self): return self.type
	def setType(self, type): self.type = type
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<RESOURCE name="%s" ID="%s" type="%s">\n' % (self.name, self.ID, self.type, ))
		level += 1
		if self.DESCRIPTION:
			self.DESCRIPTION.export(outfile, level)
		for INFO in self.INFO:
			INFO.export(outfile, level)
		for COOSYS in self.COOSYS:
			COOSYS.export(outfile, level)
		for PARAM in self.PARAM:
			PARAM.export(outfile, level)
		for LINK in self.LINK:
			LINK.export(outfile, level)
		for TABLE in self.TABLE:
			TABLE.export(outfile, level)
		for RESOURCE in self.RESOURCE:
			RESOURCE.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</RESOURCE>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('name'):
			self.name = attrs.get('name').value
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('type'):
			self.type = attrs.get('type').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DESCRIPTION':
				if len(child.childNodes)>0:
					value = child.childNodes[0].nodeValue
				else:
					value = u''
				obj = DESCRIPTION.factory(value)
				obj.build(child)
				self.setDescription(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'INFO':
				if len(child.childNodes)>0:
					text = child.childNodes[0].nodeValue
				else:
					text = u''
				obj = INFO.factory(text)
				obj.build(child)
				self.INFO.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'COOSYS':
				obj = COOSYS.factory()
				obj.build(child)
				self.COOSYS.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'PARAM':
				obj = PARAM.factory()
				obj.build(child)
				self.PARAM.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'LINK':
				obj = LINK.factory()
				obj.build(child)
				self.LINK.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'TABLE':
				obj = TABLE.factory()
				obj.build(child)
				self.TABLE.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'RESOURCE':
				obj = RESOURCE.factory()
				obj.build(child)
				self.RESOURCE.append(obj)
# end class RESOURCE


class DESCRIPTION:
	subclass = None
	def __init__(self,value):
		if type(value) == type(u''):
			self.value = value.strip()
		else:
			self.value=u''
	def factory(*args):
		if DESCRIPTION.subclass:
			return apply(DESCRIPTION.subclass, args)
		else:
			return apply(DESCRIPTION, args)
	factory = staticmethod(factory)
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<DESCRIPTION>')
		outfile.write(self.value)
		outfile.write('</DESCRIPTION>\n')
	def build(self, node_):
		attrs = node_.attributes
		for child in node_.childNodes:
			if child.nodeType == Node.TEXT_NODE and \
				 child.nodeValue != '\n':
				return child
			else:
				pass
# end class DESCRIPTION


class DEFINITIONS:
	subclass = None
	def __init__(self, COOSYS=None, PARAM=None):
		self.COOSYS = COOSYS
		self.PARAM = PARAM
	def factory(*args):
		if DEFINITIONS.subclass:
			return apply(DEFINITIONS.subclass, args)
		else:
			return apply(DEFINITIONS, args)
	factory = staticmethod(factory)
	def getCoosys(self): return self.COOSYS
	def setCoosys(self, COOSYS): self.COOSYS = COOSYS
	def getParam(self): return self.PARAM
	def setParam(self, PARAM): self.PARAM = PARAM
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<DEFINITIONS>\n')
		level += 1
		if self.COOSYS:
			self.COOSYS.export(outfile, level)
		if self.PARAM:
			self.PARAM.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</DEFINITIONS>\n')
	def build(self, node_):
		attrs = node_.attributes
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'COOSYS':
				obj = COOSYS.factory()
				obj.build(child)
				self.setCoosys(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'PARAM':
				obj = PARAM.factory()
				obj.build(child)
				self.setParam(obj)
# end class DEFINITIONS


class INFO:
	subclass = None
	def __init__(self, text, ID=None, name=None, value=''):
		self.ID = ID
		self.name = name
		self.value = value
		if type(text) == type(u''):
			self.text = text.strip()
		else:
			self.text=u''
	def factory(*args):
		if INFO.subclass:
			return apply(INFO.subclass, args)
		else:
			return apply(INFO, args)
	factory = staticmethod(factory)
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getName(self): return self.name
	def setName(self, name): self.name = name
	def getValue(self): return self.value
	def setValue(self, value): self.value = value
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<INFO ID="%s" name="%s" value="%s">' % (self.ID, self.name, self.value, ))
		outfile.write(self.text)
		outfile.write('</INFO>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('name'):
			self.name = attrs.get('name').value
		if attrs.get('value'):
			self.value = attrs.get('value').value
		for child in node_.childNodes:
			pass
# end class INFO


class PARAM:
	subclass = None
	def __init__(self, DESCRIPTION=None, VALUES=None, LINK=None, ID=None, unit=None, datatype=None, precision=None, width=None, ref=None, name=None, ucd=None, value='', arraysize=None):
		self.DESCRIPTION = DESCRIPTION
		self.VALUES = VALUES
		if LINK is None:
			self.LINK = []
		else:
			self.LINK = LINK
		self.ID = ID
		self.unit = unit
		self.datatype = datatype
		self.precision = precision
		self.width = width
		self.ref = ref
		self.name = name
		self.ucd = ucd
		self.value = value
		self.arraysize = arraysize
	def factory(*args):
		if PARAM.subclass:
			return apply(PARAM.subclass, args)
		else:
			return apply(PARAM, args)
	factory = staticmethod(factory)
	def getDescription(self): return self.DESCRIPTION
	def setDescription(self, DESCRIPTION): self.DESCRIPTION = DESCRIPTION
	def getValues(self): return self.VALUES
	def setValues(self, VALUES): self.VALUES = VALUES
	def getLink(self): return self.LINK
	def addLink(self, value): self.LINK.append(value)
	def setLink(self, index, value): self.LINK[index] = value
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getUnit(self): return self.unit
	def setUnit(self, unit): self.unit = unit
	def getDatatype(self): return self.datatype
	def setDatatype(self, datatype): self.datatype = datatype
	def getPrecision(self): return self.precision
	def setPrecision(self, precision): self.precision = precision
	def getWidth(self): return self.width
	def setWidth(self, width): self.width = width
	def getRef(self): return self.ref
	def setRef(self, ref): self.ref = ref
	def getName(self): return self.name
	def setName(self, name): self.name = name
	def getUcd(self): return self.ucd
	def setUcd(self, ucd): self.ucd = ucd
	def getValue(self): return self.value
	def setValue(self, value): self.value = value
	def getArraysize(self): return self.arraysize
	def setArraysize(self, arraysize): self.arraysize = arraysize
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<PARAM ID="%s" unit="%s" datatype="%s" precision="%s" width="%s" ref="%s" name="%s" ucd="%s" value="%s" arraysize="%s">\n' % (self.ID, self.unit, self.datatype, self.precision, self.width, self.ref, self.name, self.ucd, self.value, self.arraysize, ))
		level += 1
		if self.DESCRIPTION:
			self.DESCRIPTION.export(outfile, level)
		if self.VALUES:
			self.VALUES.export(outfile, level)
		for LINK in self.LINK:
			LINK.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</PARAM>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('unit'):
			self.unit = attrs.get('unit').value
		if attrs.get('datatype'):
			self.datatype = attrs.get('datatype').value
		if attrs.get('precision'):
			self.precision = attrs.get('precision').value
		if attrs.get('width'):
			self.width = attrs.get('width').value
		if attrs.get('ref'):
			self.ref = attrs.get('ref').value
		if attrs.get('name'):
			self.name = attrs.get('name').value
		if attrs.get('ucd'):
			self.ucd = attrs.get('ucd').value
		if attrs.get('value'):
			self.value = attrs.get('value').value
		if attrs.get('arraysize'):
			self.arraysize = attrs.get('arraysize').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DESCRIPTION':
				if len(child.childNodes)>0:
					value = child.childNodes[0].nodeValue
				else:
					value = u''

				obj = DESCRIPTION.factory(value)
				obj.build(child)
				self.setDescription(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'VALUES':
				obj = VALUES.factory()
				obj.build(child)
				self.VALUES.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'LINK':
				obj = LINK.factory()
				obj.build(child)
				self.LINK.append(obj)
# end class PARAM


class TABLE:
	subclass = None
	def __init__(self, DESCRIPTION=None, FIELD=None, LINK=None, DATA=None, ID=None, name=None, ref=None):
		self.DESCRIPTION = DESCRIPTION
		if FIELD is None:
			self.FIELD = []
		else:
			self.FIELD = FIELD
		if LINK is None:
			self.LINK = []
		else:
			self.LINK = LINK
		self.DATA = DATA
		self.ID = ID
		self.name = name
		self.ref = ref
	def factory(*args):
		if TABLE.subclass:
			return apply(TABLE.subclass, args)
		else:
			return apply(TABLE, args)
	factory = staticmethod(factory)
	def getDescription(self): return self.DESCRIPTION
	def setDescription(self, DESCRIPTION): self.DESCRIPTION = DESCRIPTION
	def getField(self): return self.FIELD
	def addField(self, value): self.FIELD.append(value)
	def setField(self, index, value): self.FIELD[index] = value
	def getLink(self): return self.LINK
	def addLink(self, value): self.LINK.append(value)
	def setLink(self, index, value): self.LINK[index] = value
	def getData(self): return self.DATA
	def setData(self, DATA): self.DATA = DATA
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getName(self): return self.name
	def setName(self, name): self.name = name
	def getRef(self): return self.ref
	def setRef(self, ref): self.ref = ref
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<TABLE ID="%s" name="%s" ref="%s">\n' % (self.ID, self.name, self.ref, ))
		level += 1
		if self.DESCRIPTION:
			self.DESCRIPTION.export(outfile, level)
		for FIELD in self.FIELD:
			FIELD.export(outfile, level)
		for LINK in self.LINK:
			LINK.export(outfile, level)
		if self.DATA:
			self.DATA.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</TABLE>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('name'):
			self.name = attrs.get('name').value
		if attrs.get('ref'):
			self.ref = attrs.get('ref').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DESCRIPTION':
				if len(child.childNodes)>0:
					value = child.childNodes[0].nodeValue
				else:
					value = u''

				obj = DESCRIPTION.factory(value)
				obj.build(child)
				self.setDescription(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'FIELD':
				obj = FIELD.factory()
				obj.build(child)
				self.FIELD.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'LINK':
				obj = LINK.factory()
				obj.build(child)
				self.LINK.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DATA':
				obj = DATA.factory()
				obj.build(child)
				self.setData(obj)
# end class TABLE


class FIELD:
	subclass = None
	def __init__(self, DESCRIPTION=None, VALUES=None, LINK=None, ID=None, unit=None, datatype=None, precision=None, width=None, ref=None, name=None, ucd='', arraysize='', type=''):
		self.DESCRIPTION = DESCRIPTION
		if VALUES is None:
			self.VALUES = []
		else:
			self.VALUES = VALUES
		self.LINK = LINK
		self.ID = ID
		self.unit = unit
		self.datatype = datatype
		self.precision = precision
		self.width = width
		self.ref = ref
		self.name = name
		self.ucd = ucd
		self.arraysize = arraysize
		self.type = type
	def factory(*args):
		if FIELD.subclass:
			return apply(FIELD.subclass, args)
		else:
			return apply(FIELD, args)
	factory = staticmethod(factory)
	def getDescription(self): return self.DESCRIPTION
	def setDescription(self, DESCRIPTION): self.DESCRIPTION = DESCRIPTION
	def getValues(self): return self.VALUES
	def addValues(self, value): self.VALUES.append(value)
	def setValues(self, index, value): self.VALUES[index] = value
	def getLink(self): return self.LINK
	def setLink(self, LINK): self.LINK = LINK
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getUnit(self): return self.unit
	def setUnit(self, unit): self.unit = unit
	def getDatatype(self): return self.datatype
	def setDatatype(self, datatype): self.datatype = datatype
	def getPrecision(self): return self.precision
	def setPrecision(self, precision): self.precision = precision
	def getWidth(self): return self.width
	def setWidth(self, width): self.width = width
	def getRef(self): return self.ref
	def setRef(self, ref): self.ref = ref
	def getName(self): return self.name
	def setName(self, name): self.name = name
	def getUcd(self): return self.ucd
	def setUcd(self, ucd): self.ucd = ucd
	def getArraysize(self): return self.arraysize
	def setArraysize(self, arraysize): self.arraysize = arraysize
	def getType(self): return self.type
	def setType(self, type): self.type = type
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<FIELD ID="%s" unit="%s" datatype="%s" precision="%s" width="%s" ref="%s" name="%s" ucd="%s" arraysize="%s" type="%s">\n' % (self.ID, self.unit, self.datatype, self.precision, self.width, self.ref, self.name, self.ucd, self.arraysize, self.type, ))
		level += 1
		if self.DESCRIPTION:
			self.DESCRIPTION.export(outfile, level)
		for VALUES in self.VALUES:
			VALUES.export(outfile, level)
		if self.LINK:
			self.LINK.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</FIELD>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('unit'):
			self.unit = attrs.get('unit').value
		if attrs.get('datatype'):
			self.datatype = attrs.get('datatype').value
		if attrs.get('precision'):
			self.precision = attrs.get('precision').value
		if attrs.get('width'):
			self.width = attrs.get('width').value
		if attrs.get('ref'):
			self.ref = attrs.get('ref').value
		if attrs.get('name'):
			self.name = attrs.get('name').value
		if attrs.get('ucd'):
			self.ucd = attrs.get('ucd').value
		if attrs.get('arraysize'):
			self.arraysize = attrs.get('arraysize').value
		if attrs.get('type'):
			self.type = attrs.get('type').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'DESCRIPTION':
				if len(child.childNodes)>0:
					value = child.childNodes[0].nodeValue
				else:
					value = u''

				obj = DESCRIPTION.factory(value)
				obj.build(child)
				self.setDescription(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'VALUES':
				obj = VALUES.factory()
				obj.build(child)
				self.VALUES.append(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'LINK':
				obj = LINK.factory()
				obj.build(child)
				self.setLink(obj)
# end class FIELD


class VALUES:
	subclass = None
	def __init__(self, MIN=None, MAX=None, OPTION=None, ID=None, type='', null=None, invalid=None):
		self.MIN = MIN
		self.MAX = MAX
		if OPTION is None:
			self.OPTION = []
		else:
			self.OPTION = OPTION
		self.ID = ID
		self.type = type
		self.null = null
		self.invalid = invalid
	def factory(*args):
		if VALUES.subclass:
			return apply(VALUES.subclass, args)
		else:
			return apply(VALUES, args)
	factory = staticmethod(factory)
	def getMin(self): return self.MIN
	def setMin(self, MIN): self.MIN = MIN
	def getMax(self): return self.MAX
	def setMax(self, MAX): self.MAX = MAX
	def getOption(self): return self.OPTION
	def addOption(self, value): self.OPTION.append(value)
	def setOption(self, index, value): self.OPTION[index] = value
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getType(self): return self.type
	def setType(self, type): self.type = type
	def getNull(self): return self.null
	def setNull(self, null): self.null = null
	def getInvalid(self): return self.invalid
	def setInvalid(self, invalid): self.invalid = invalid
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<VALUES ID="%s" type="%s" null="%s" invalid="%s">\n' % (self.ID, self.type, self.null, self.invalid, ))
		level += 1
		if self.MIN:
			self.MIN.export(outfile, level)
		if self.MAX:
			self.MAX.export(outfile, level)
		for OPTION in self.OPTION:
			OPTION.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</VALUES>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('type'):
			self.type = attrs.get('type').value
		if attrs.get('null'):
			self.null = attrs.get('null').value
		if attrs.get('invalid'):
			self.invalid = attrs.get('invalid').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'MIN':
				obj = MIN.factory()
				obj.build(child)
				self.setMin(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'MAX':
				obj = MAX.factory()
				obj.build(child)
				self.setMax(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'OPTION':
				obj = OPTION.factory()
				obj.build(child)
				self.OPTION.append(obj)
# end class VALUES


class MIN:
	subclass = None
	def __init__(self, value='', inclusive=None):
		self.value = value
		self.inclusive = inclusive
	def factory(*args):
		if MIN.subclass:
			return apply(MIN.subclass, args)
		else:
			return apply(MIN, args)
	factory = staticmethod(factory)
	def getValue(self): return self.value
	def setValue(self, value): self.value = value
	def getInclusive(self): return self.inclusive
	def setInclusive(self, inclusive): self.inclusive = inclusive
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<MIN value="%s" inclusive="%s">\n' % (self.value, self.inclusive, ))
		level += 1
		level -= 1
		showIndent(outfile, level)
		outfile.write('</MIN>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('value'):
			self.value = attrs.get('value').value
		if attrs.get('inclusive'):
			self.inclusive = attrs.get('inclusive').value
		for child in node_.childNodes:
			pass
# end class MIN


class MAX:
	subclass = None
	def __init__(self, value='', inclusive=None):
		self.value = value
		self.inclusive = inclusive
	def factory(*args):
		if MAX.subclass:
			return apply(MAX.subclass, args)
		else:
			return apply(MAX, args)
	factory = staticmethod(factory)
	def getValue(self): return self.value
	def setValue(self, value): self.value = value
	def getInclusive(self): return self.inclusive
	def setInclusive(self, inclusive): self.inclusive = inclusive
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<MAX value="%s" inclusive="%s">\n' % (self.value, self.inclusive, ))
		level += 1
		level -= 1
		showIndent(outfile, level)
		outfile.write('</MAX>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('value'):
			self.value = attrs.get('value').value
		if attrs.get('inclusive'):
			self.inclusive = attrs.get('inclusive').value
		for child in node_.childNodes:
			pass
# end class MAX


class OPTION:
	subclass = None
	def __init__(self, OPTION=None, name=None, value=''):
		if OPTION is None:
			self.OPTION = []
		else:
			self.OPTION = OPTION
		self.name = name
		self.value = value
	def factory(*args):
		if OPTION.subclass:
			return apply(OPTION.subclass, args)
		else:
			return apply(OPTION, args)
	factory = staticmethod(factory)
	def getOption(self): return self.OPTION
	def addOption(self, value): self.OPTION.append(value)
	def setOption(self, index, value): self.OPTION[index] = value
	def getName(self): return self.name
	def setName(self, name): self.name = name
	def getValue(self): return self.value
	def setValue(self, value): self.value = value
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<OPTION name="%s" value="%s">\n' % (self.name, self.value, ))
		level += 1
		for OPTION in self.OPTION:
			OPTION.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</OPTION>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('name'):
			self.name = attrs.get('name').value
		if attrs.get('value'):
			self.value = attrs.get('value').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'OPTION':
				obj = OPTION.factory()
				obj.build(child)
				self.OPTION.append(obj)
# end class OPTION


class LINK:
	subclass = None
	def __init__(self, ID=None, contentRole='', contentType=None, title='', value='', href=None, gref=None, action=None):
		self.ID = ID
		self.contentRole = contentRole
		self.contentType = contentType
		self.title = title
		self.value = value
		self.href = href
		self.gref = gref
		self.action = action
	def factory(*args):
		if LINK.subclass:
			return apply(LINK.subclass, args)
		else:
			return apply(LINK, args)
	factory = staticmethod(factory)
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getContentRole(self): return self.contentRole
	def setContentRole(self, contentRole): self.contentRole = contentRole
	def getContentType(self): return self.contentType
	def setContentType(self, contentType): self.contentType = contentType
	def getTitle(self): return self.title
	def setTitle(self, title): self.title = title
	def getValue(self): return self.value
	def setValue(self, value): self.value = value
	def getHref(self): return self.href
	def setHref(self, href): self.href = href
	def getGref(self): return self.gref
	def setGref(self, gref): self.gref = gref
	def getAction(self): return self.action
	def setAction(self, action): self.action = action
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<LINK ID="%s" content-role="%s" content-type="%s" title="%s" value="%s" href="%s" gref="%s" action="%s">\n' % (self.ID, self.contentRole, self.contentType, self.title, self.value, self.href, self.gref, self.action, ))
		level += 1
		level -= 1
		showIndent(outfile, level)
		outfile.write('</LINK>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('content-role'):
			self.contentRole = attrs.get('content-role').value
		if attrs.get('content-type'):
			self.contentType = attrs.get('content-type').value
		if attrs.get('title'):
			self.title = attrs.get('title').value
		if attrs.get('value'):
			self.value = attrs.get('value').value
		if attrs.get('href'):
			self.href = attrs.get('href').value
		if attrs.get('gref'):
			self.gref = attrs.get('gref').value
		if attrs.get('action'):
			self.action = attrs.get('action').value
		for child in node_.childNodes:
			pass
# end class LINK


class DATA:
	subclass = None
	def __init__(self, TABLEDATA=None, BINARY=None, FITS=None):
		self.TABLEDATA = TABLEDATA
		self.BINARY = BINARY
		self.FITS = FITS
	def factory(*args):
		if DATA.subclass:
			return apply(DATA.subclass, args)
		else:
			return apply(DATA, args)
	factory = staticmethod(factory)
	def getTabledata(self): return self.TABLEDATA
	def setTabledata(self, TABLEDATA): self.TABLEDATA = TABLEDATA
	def getBinary(self): return self.BINARY
	def setBinary(self, BINARY): self.BINARY = BINARY
	def getFits(self): return self.FITS
	def setFits(self, FITS): self.FITS = FITS
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<DATA>\n')
		level += 1
		if self.TABLEDATA:
			self.TABLEDATA.export(outfile, level)
		if self.BINARY:
			self.BINARY.export(outfile, level)
		if self.FITS:
			self.FITS.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</DATA>\n')
	def build(self, node_):
		attrs = node_.attributes
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'TABLEDATA':
				obj = TABLEDATA.factory()
				obj.build(child)
				self.setTabledata(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'BINARY':
				obj = BINARY.factory()
				obj.build(child)
				self.setBinary(obj)
			elif child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'FITS':
				obj = FITS.factory()
				obj.build(child)
				self.setFits(obj)
# end class DATA


class TABLEDATA:
	subclass = None
	def __init__(self, TR=None):
		if TR is None:
			self.TR = []
		else:
			self.TR = TR
	def factory(*args):
		if TABLEDATA.subclass:
			return apply(TABLEDATA.subclass, args)
		else:
			return apply(TABLEDATA, args)
	factory = staticmethod(factory)
	def getTr(self): return self.TR
	def addTr(self, value): self.TR.append(value)
	def setTr(self, index, value): self.TR[index] = value
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<TABLEDATA>\n')
		level += 1
		for TR in self.TR:
			TR.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</TABLEDATA>\n')
	def build(self, node_):
		attrs = node_.attributes
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'TR':
				obj = TR.factory()
				obj.build(child)
				self.TR.append(obj)
# end class TABLEDATA


class TD:
	subclass = None
	def __init__(self, value,ref=None):
		self.ref = ref
		if type(value) == type(u''):
			self.value = value.strip()
		else:
			self.value=u''
	def factory(*args):
		if TD.subclass:
			return apply(TD.subclass, args)
		else:
			return apply(TD, args)
	factory = staticmethod(factory)
	def getRef(self): return self.ref
	def setRef(self, ref): self.ref = ref
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<TD ref="%s">' % (self.ref, ))
		outfile.write(self.value)
		outfile.write('</TD>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ref'):
			self.ref = attrs.get('ref').value
		for child in node_.childNodes:
			pass
# end class TD


class TR:
	subclass = None
	def __init__(self, TD=None):
		if TD is None:
			self.TD = []
		else:
			self.TD = TD
	def factory(*args):
		if TR.subclass:
			return apply(TR.subclass, args)
		else:
			return apply(TR, args)
	factory = staticmethod(factory)
	def getTd(self): return self.TD
	def addTd(self, value): self.TD.append(value)
	def setTd(self, index, value): self.TD[index] = value
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<TR>\n')
		level += 1
		for TD in self.TD:
			TD.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</TR>\n')
	def build(self, node_):
		attrs = node_.attributes
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'TD':
				if len(child.childNodes)>0:
					value = child.childNodes[0].nodeValue
				else:
					value = u''

				obj = TD.factory(value)
				obj.build(child)
				self.TD.append(obj)
# end class TR


class FITS:
	subclass = None
	def __init__(self, STREAM=None, extnum=None):
		self.STREAM = STREAM
		self.extnum = extnum
	def factory(*args):
		if FITS.subclass:
			return apply(FITS.subclass, args)
		else:
			return apply(FITS, args)
	factory = staticmethod(factory)
	def getStream(self): return self.STREAM
	def setStream(self, STREAM): self.STREAM = STREAM
	def getExtnum(self): return self.extnum
	def setExtnum(self, extnum): self.extnum = extnum
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<FITS extnum="%s">\n' % (self.extnum, ))
		level += 1
		if self.STREAM:
			self.STREAM.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</FITS>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('extnum'):
			self.extnum = attrs.get('extnum').value
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'STREAM':
				obj = STREAM.factory()
				obj.build(child)
				self.setStream(obj)
# end class FITS


class BINARY:
	subclass = None
	def __init__(self, STREAM=None):
		self.STREAM = STREAM
	def factory(*args):
		if BINARY.subclass:
			return apply(BINARY.subclass, args)
		else:
			return apply(BINARY, args)
	factory = staticmethod(factory)
	def getStream(self): return self.STREAM
	def setStream(self, STREAM): self.STREAM = STREAM
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<BINARY>\n')
		level += 1
		if self.STREAM:
			self.STREAM.export(outfile, level)
		level -= 1
		showIndent(outfile, level)
		outfile.write('</BINARY>\n')
	def build(self, node_):
		attrs = node_.attributes
		for child in node_.childNodes:
			if child.nodeType == Node.ELEMENT_NODE and \
				child.nodeName == 'STREAM':
				obj = STREAM.factory()
				obj.build(child)
				self.setStream(obj)
# end class BINARY


class STREAM:
	subclass = None
	def __init__(self, type='', href=None, actuate='', encoding='', expires=None, rights=None):
		self.type = type
		self.href = href
		self.actuate = actuate
		self.encoding = encoding
		self.expires = expires
		self.rights = rights
	def factory(*args):
		if STREAM.subclass:
			return apply(STREAM.subclass, args)
		else:
			return apply(STREAM, args)
	factory = staticmethod(factory)
	def getType(self): return self.type
	def setType(self, type): self.type = type
	def getHref(self): return self.href
	def setHref(self, href): self.href = href
	def getActuate(self): return self.actuate
	def setActuate(self, actuate): self.actuate = actuate
	def getEncoding(self): return self.encoding
	def setEncoding(self, encoding): self.encoding = encoding
	def getExpires(self): return self.expires
	def setExpires(self, expires): self.expires = expires
	def getRights(self): return self.rights
	def setRights(self, rights): self.rights = rights
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<STREAM type="%s" href="%s" actuate="%s" encoding="%s" expires="%s" rights="%s">\n' % (self.type, self.href, self.actuate, self.encoding, self.expires, self.rights, ))
		level += 1
		level -= 1
		showIndent(outfile, level)
		outfile.write('</STREAM>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('type'):
			self.type = attrs.get('type').value
		if attrs.get('href'):
			self.href = attrs.get('href').value
		if attrs.get('actuate'):
			self.actuate = attrs.get('actuate').value
		if attrs.get('encoding'):
			self.encoding = attrs.get('encoding').value
		if attrs.get('expires'):
			self.expires = attrs.get('expires').value
		if attrs.get('rights'):
			self.rights = attrs.get('rights').value
		for child in node_.childNodes:
			pass
# end class STREAM


class COOSYS:
	subclass = None
	def __init__(self, ID=None, equinox=None, epoch=None, system=''):
		self.ID = ID
		self.equinox = equinox
		self.epoch = epoch
		self.system = system
	def factory(*args):
		if COOSYS.subclass:
			return apply(COOSYS.subclass, args)
		else:
			return apply(COOSYS, args)
	factory = staticmethod(factory)
	def getId(self): return self.ID
	def setId(self, ID): self.ID = ID
	def getEquinox(self): return self.equinox
	def setEquinox(self, equinox): self.equinox = equinox
	def getEpoch(self): return self.epoch
	def setEpoch(self, epoch): self.epoch = epoch
	def getSystem(self): return self.system
	def setSystem(self, system): self.system = system
	def __repr__(self):
		catch = StringIO.StringIO('')
		self.export(catch,0)
		catch.seek(0,0)
		all=catch.read()
		del(catch)
		return all
	def export(self, outfile, level):
		showIndent(outfile, level)
		outfile.write('<COOSYS ID="%s" equinox="%s" epoch="%s" system="%s">\n' % (self.ID, self.equinox, self.epoch, self.system, ))
		level += 1
		level -= 1
		showIndent(outfile, level)
		outfile.write('</COOSYS>\n')
	def build(self, node_):
		attrs = node_.attributes
		if attrs.get('ID'):
			self.ID = attrs.get('ID').value
		if attrs.get('equinox'):
			self.equinox = attrs.get('equinox').value
		if attrs.get('epoch'):
			self.epoch = attrs.get('epoch').value
		if attrs.get('system'):
			self.system = attrs.get('system').value
		for child in node_.childNodes:
			pass
# end class COOSYS


USAGE_TEXT = """
Usage: python -i <Parser>.py <in_xml_file>
"""

def usage():
	print USAGE_TEXT
	sys.exit(-1)


def parse(inFileName):
	doc = minidom.parse(inFileName)
	for node in doc.childNodes:
		if node.nodeType==node.ELEMENT_NODE:
			rootNode = node
			break
	if rootNode.nodeName != 'VOTABLE':
		errMsg = "XML_NOT_VOTABLE: Not a VOTABLE document. Root element found: "+\
			str(rootNode.nodeName)
		raise Exception, errMsg
#        sys.stdout.write("Not a VOTABLE document!\n")
#        sys.stdout.write("Root node name: "+str(rootNode.nodeName)+"\n")
		return -1
	rootObj = VOTABLE.factory()
	rootObj.build(rootNode)
	# Enable Python to collect the space used by the DOM.
	doc = None
#    sys.stdout.write('<?xml version="1.0" ?>\n')
#    rootObj.export(sys.stdout, 0)
	return rootObj


def parseString(inString):
	doc = minidom.parseString(inString)
	for node in doc.childNodes:
		if node.nodeType==node.ELEMENT_NODE:
			rootNode = node
			break
	if rootNode.nodeName != 'VOTABLE':
		errMsg = "XML_NOT_VOTABLE: Not a VOTABLE document. Root element found: "+\
			str(rootNode.nodeName)
		raise Exception, errMsg
#        sys.stdout.write("Not a VOTABLE document!\n")
#        sys.stdout.write("Root node name: "+str(rootNode.nodeName)+"\n")
		return -1
	rootObj = VOTABLE.factory()
	rootObj.build(rootNode)
	# Enable Python to collect the space used by the DOM.
	doc = None
#    sys.stdout.write('<?xml version="1.0" ?>\n')
#    rootObj.export(sys.stdout, 0)
	return rootObj


def printFields(votable):
	"""
	Convenience function to print the field names, descriptions and formats
	of a votable instance.

	INPUT: votable  instance, result of parsing a VOTable.
	"""
	totwidth = 0
	ii = 0
	for f in votable.RESOURCE[0].TABLE[0].FIELD:
		ii += 1
		prec = '  '
		if f.datatype=='char' and f.arraysize=='':
				width=1
				totwidth += 1
		elif f.datatype=='char' and f.arraysize!='':
				if f.arraysize[-1] == '*':
					width=f.arraysize[:-1]
					totwidth += int(f.arraysize[:-1])
				else:
					width=f.arraysize
					totwidth += int(f.arraysize)
		elif f.datatype!='char' and f.arraysize=='':
				if f.width is not None:
					width = f.width
					totwidth += int(f.width)
				else:
					width = 0
					totwidth += width
				if f.precision is not None:
					prec = '.'+f.precision
		if f.DESCRIPTION is not None:
			descr = f.DESCRIPTION.value[:40]
			if len(f.DESCRIPTION.value) > 40:
				descr += '\\'
		else:
			descr = u''
		print "%3d %-10s %-42s  %5s%s %-7s %-5s" % (ii,f.getName(),descr,\
												width,prec,\
												f.datatype,f.arraysize)
	return

def printTables(votable):
	"""
	"""
	for rr in range(len(root.RESOURCE)):
		for tt in range(len(root.RESOURCE[rr].TABLE)):
			print '<TABLE number=%d>' % (rr+tt+1)
			fnames=[]
			for f in root.RESOURCE[rr].TABLE[tt].FIELD:
				fn = re.sub ("[^\w0-9]", '_',f.name)
				fnames.append(fn)
			jj=1
			ii=0
			for r in root.RESOURCE[rr].TABLE[tt].DATA.TABLEDATA.TR:
				print '<TR number=%d>' % (jj)
				for c in r.TD:
					print '<' + fnames[ii] + '>' + c.value + '</' + fnames[ii] + '>'
					ii+=1
				ii=0
				print '</TR>\n'
				jj+=1
			print '</TABLE>'
			print

def formatCode(field):
	"""
	Defines the mapping between the VOTable
	"""
	types={'boolean':'c','bit':'c','unsignedbyte':'u','short':'d','int':'d',\
		   'long':'d','char':'s','unicodechar':'r',\
		   'float':'f','double':'f','floatcomplex':'s','doublecomplex':'s'}

	if type(field.datatype) != type(''):
		format = ''

	datatype = str(field.datatype.lower())
	if field.width == None:
		width = 0
	else:
		width = field.width
	if field.precision == None:
		prec = 0
	else:
		prec = field.precision
	if types.has_key(datatype.lower()):
		format = types[datatype]
		if width > 0 and prec == 0:
			format = '%' + str(width) + format
		if prec > 0 and width > 0:
			format = '%' + str(width) + '.' + str(prec) + format
		if format == 's' and field.arraysize != "":
			if field.arraysize[-1] == '*':
				width=field.arraysize[:-1]
			else:
				width = field.arraysize
			format = '%' + str(width) + 's'
	else:
		format = ''

	return format



def createCatReaderRecord(votable):
	"""
	Create the description dictionary for CatReader.

	NOTE: VOTable usually does not contain all the information
	necessary to fill the records. There are no standard tags in
	VOTable to describe offset and scaling of catalogue values.
	"""
	Record = {}
	ii = 0

	for field in votable.RESOURCE[0].TABLE[0].FIELD:
		Record.update({field.name:{'pos':ii,'datatype':str(field.datatype),'off':None,
								   'scale':None,'format':formatCode(field)}})
		ii+=1

	return Record


def main():
	"""
	Usage: python -i VOTable.py <votable.xml>

	This will parse the votable and return to the python prompt with a new instance called 'root'.
	This instance can then be used for further processing.
	"""
	args = sys.argv[1:]
	if len(args) != 1:
		usage()
	return parse(args[0])



if __name__ == '__main__':
	root = main()
	printTables(root)

