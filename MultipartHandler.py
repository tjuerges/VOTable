#!/usr/bin/env python
# @(#) $Id: MultipartHandler.py,v 1.5 2005/10/17 15:51:45 tjuerges Exp $
#
#  ALMA - Atacama Large Millimiter Array
#  (c) European Southern Observatory, 2002
#  Copyright by ESO (in the framework of the ALMA collaboration),
#  All rights reserved
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2.1 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston,
#  MA 02111-1307  USA
#
import sys, types

import email
from email.Parser import Parser
import struct

# the following does not work if there is only
# minidom:
#from xml.dom import implementation

from xml.dom.minidom import getDOMImplementation
implementation = getDOMImplementation()

# adjust and uncomment if necessary
# sys.path.append('/home/awicenec/VO/VOTable')

import VOTable

__HELP__="""
Parser for multipart/related messages with one part being a VOTable
which is referring to binary attachments.

SYNOPSIS: python -i MultipartHandler.py -f <MultipartMessage> [-r <ResourceName>] [-c <contentID>]

where <MultipartMessage> is the name of an ALMA multipart/related mime message file
and <ResourceName> is an optional parameter to select one Resource according to
the name or ID attribute.

HISTORY: Andreas Wicenec [ESO, Aug/Sep 2003]
		 0.11, 05-Sep-2003: fallback to minidom
		 0.12, 05-Sep-2003: python version incompatibilities
		 0.13, 23-Mar-2005: updated to deal with new MonitorStore files
"""

def usage():
	print __HELP__

def getMimeParts(msg, mime):
	"""
	Get the part of the email with the specified mime-type
	"""
	parts = []
	for m in msg.walk():
		mtype = m.get('Content-Type').split(';')[0]
#        mtype = m.get_content_type()  new in version 2.2.2
		if mtype == mime:
			parts.append(m)
# return full mime part instead of contents
#            parts.append(m.get_payload())

	return parts


def getPartId(msg, contentId):
	"""
	Return the part which belongs to the contentId given.
	"""
	for m in msg.walk():
		cID = m.get('Content-ID')
		if type(cID) == type('') and cID[1:-1] == contentId:
			return m
	return -1

def votypToForm(votyp, arraysize):
	"""
	Function provides the mapping between the VO data-type strings and the
	charcters used by the struct function for the conversion. It takes the
	VO-type strings and the arraysize on input and returns a tuple: the format code
	and two flags which are used to control the unpacking of the arrays.

	Synopsis: (forms, mult, compl) = votypToForm(votyp, arraysize)

	Input:
		votyp:        VOTable type string
		arraysize:    VOTable arraysize specification string of form '[N,M]' where
					  N and M are integer values.

	Output:
		(forms, mult, compl):    tuple with three values, forms is a string, mult
								 is an integer and compl is either 1 or 2.
	"""
	# for multi-dimension arrays, enforce the format [N,M]
	if arraysize.find('x') > -1:
		arraysize = arraysize.replace('x',',')
		arraysize = '['+arraysize+']'

	if arraysize.find(',') > -1:
		as = arraysize.split(',')
		N = as[0]
		N = N[1:]
		M = as[1]
		M = M[:len(M)-1]
		arraysize = int(N)*int(M)

	arraysize = [long(arraysize)]

	mult = 0
	compl = 1

	if votyp == 'char' and arraysize[0] == 1:
		form = 'c'
	elif votyp == 'short':
		form = 'h'
	elif votyp == 'int':
		form = 'i'
	elif votyp == 'long':
		form = 'l'
	elif votyp == 'unsigned long':
			form = 'L'
	elif votyp == 'long long':
			form = 'q'
	elif votyp == 'unsigned long long':
			form = 'Q'
	elif votyp == 'float':
		form = 'f'
	elif votyp == 'double':
			form = 'd'
	elif votyp == 'char' and arraysize[0] > 1:
		form = 's'
	elif votyp == 'floatComplex':
		form = 'f'
		compl = 2
	else:
		return ([-1],-1,-1)

	forms = []
	for as in arraysize:
		if as > 1:
			mult += as
			forms.append(str(compl*as)+form)
		elif len(arraysize) == 1:
			mult = 1
			forms.append(str(compl)+form)

	return (forms,mult,compl)


def unpackBinary(votyp, arraysize, binstr):
	"""
	Function provides the mapping between the format codes
	in the VOTable definition and the struct.unpack function.

	It does the unpacking of a binary-string <binstr> according
	to the description contained in the associated FIELD element.

	In case the format does not match the size of the string -2
	is returned. In case the format found in field.datatype is
	unknown to this function -1 is returned.
	"""
	(forms,mult,compl) = votypToForm(votyp, arraysize)
	if forms[0] == -1:
		print "ERROR: Unable to decode type: %s" % votyp
		sys.exit()


# TODO: solve the issue with multidimensional arrays
	expected = 0
	for ff in forms:
		expected += struct.calcsize(ff)

	if expected != len(binstr):
		return (expected,len(binstr))
	else:
		if (sys.byteorder == "little") and (forms[0] != "c") and (forms[0] != "s"):
			ubin = struct.unpack("!"+forms[0],binstr)
		else:
			ubin = struct.unpack(forms[0],binstr)
		if mult*compl > 1:
			cbin = []

			if compl == 2:
				for kk in xrange(0,mult*compl,2):
					cbin.append(complex(ubin[kk],ubin[kk+1]))
			else:
				cbin.append(ubin[:mult*compl])
			bin = cbin
		else:
			bin = ubin
		return bin


def interpretVotable(root, selection='', verbose=0):
	"""
	"""
	cidI = {}
	cids = []
	res = []

#	print root

	for resource in root.RESOURCE:
		if selection != '':
			if resource.name == selection or resource.ID == selection:
				res = [resource]
		else:
			res.append(resource)

	for r in res:
#		print r.TABLE[0].name

#
# TODO: expand for multiple tables.
#
		for field in r.TABLE[0].FIELD:

#			print field


	# do it only if the FIELD type == 'reference'
			if field.type == 'reference':

	# deal with optional SCALE and ZERO OPTIONS

				zero = 0.
				scale = 1.

				for V in field.VALUES:
					for O in V.OPTION:
						if O.name == 'ZERO':
							zero = eval(O.value.replace('e','**'))
						elif O.name == 'SCALE':
							scale = eval(O.value.replace('e','**'))


	# make sure that there is a LINK element
				try:
					(prefix,ref)=field.LINK.href.split(':',1)  # take everything after the first ':'
					cidI.update({ref:[field.datatype, field.arraysize, zero, scale]})
					cids.append(ref)
					if verbose:
						print "Found data: Qualifier %s" % ref
				except:
					errormsg = "FIELD type is reference but " + \
						  "no valid LINK element found!"
					if verbose:
						print errormsg
						print field
					prefix = ''


	return (res, cidI, cids)




def createVotableDOM(votable=''):
	"""
	Return a VOTable DOM. The votable string on input may contain
	a complete VOTable document string which will be imported into
	the DOM.
	"""
	try:
		rootname = votable.__nodeName
		if rootname.upper() != 'VOTABLE':
			raise 'Wrong root element:'+rootname
		else:
			doctype=implementation.createDocumentType('VOTABLE','','VOTABLE.dtd')
			newdoc=implementation.createDocument(None,"",doctype)
			voTable = newdoc.importNode(votable,deep=1)
			newdoc.appendChild(voTable)
	except:
		doctype=implementation.createDocumentType('VOTABLE','','VOTABLE.dtd')
		newdoc=implementation.createDocument(None,"VOTABLE",doctype)
		voTable=newdoc.documentElement
	return newdoc



def constructMessage(votable, res, binParts):
	"""
	Construct a new message
	"""
	# all resources to be returned have to be in the res list all additional ones
	# (if any) are deleted.
	del(votable.RESOURCE[len(res):])
	for ii in range(len(res)):
		votable.setResource(ii,res[ii])
	xmlPart = email.Message.Message()
	xmlPart.set_payload(votable.__repr__())
	newmsg = email.Message.Message()
	newmsg.set_type('multipart/related')
	newmsg.attach(xmlPart)
	for b in binParts:
		newmsg.attach(b)

	return newmsg



def interpretBinary(msg, cids, cidI, verbose=0):
	"""
	Function does the interpretation of the binary attachments according to the
	information stored in the cids dictionary.
	"""
	binParts = []
	binarr = []
	for ref in cids:
		part = getPartId(msg, ref)
		if type(part) == types.IntType:
			print "ERROR: cid %s not found!" % ref
			sys.exit()
		binParts.append(part)
		sbin = part.get_payload()
		ct = part.get('Content-Type').split(';')[0]
	#                ct = part.get_content_type() new in version 2.2.2
		if ct == 'binary/octet-stream':
			if verbose:
				print 'Decoding binary attachment: ',ref
			datatype = cidI[ref][0]
			arraysize = cidI[ref][1]
			try:
				bin = unpackBinary(datatype, arraysize, sbin)
				if verbose:
					print bin
			except:
				errormsg = "ERROR: Expected size of binary attachment %s does not match the "+\
					"actual size! (expected, actual) = %s" % (ref, str(bin))
				raise Exception, errormsg
			# apply scale and zero
			zero = cidI[ref][2]
			scale = cidI[ref][3]

			if type(bin) == type([]):
				bin = bin[0]
			sbin = tuple(map(lambda x:x*scale+zero,bin))
		else:
			sbin = tuple(map(lambda x:x*scale+zero,sbin))
		binarr.append(sbin)
		if verbose:
			print sbin
			print

	return (binParts, binarr)



def main(args):
	"""
	"""
	import getopt

	args = sys.argv[1:]
	opts,args = getopt.getopt(args,"f:r:c:",\
			   ["file","resource","cid"])

	if len(opts) == 0:
		usage()
		sys.exit()

	mailFile = ''
	ResourceName = ''
	cid = ''

	for o,v in opts:
		if o in ['-f','--file']:
			mailFile = v
		if o in ['-r','--resource']:
			ResourceName = v
		if o in ['-c','--cid']:
			cid = v
	try:
		f=open(mailFile)
	except:
		print "Unable to open ",mailFile
		usage()

	eP = Parser()  # instantiate the email Parser
	try:
		msg = eP.parse(f) # and parse the file
		f.close()
	except Exception, e:
		print "email parsing failed: " + str(e)
		return ''

	# if cid is specified just return that part and bail out
	if len(cid) > 0:
		part = getPartId(msg, cid)
		return part
	# get the first xml part of the email...
	xmlParts = getMimeParts(msg, 'text/xml')
	if len(xmlParts) == 0:
		print "No text/xml part found!"
		return ''
	else:
		xml = xmlParts[0].get_payload()  # There should only be one!


	# ...and parse it
	try:
		root = VOTable.parseString(xml)
	except Exception, e:
#        print xml
		print "XML parsing failed! " + str(e)
		return ''

	(res, cidI, cids) = interpretVotable(root, selection=ResourceName)

	try:
		(binParts, binarr) = interpretBinary(msg, cids, cidI)
	except Exception, e:
		print "Interpretation of binary attachment failed: " + str(e)
		return ''

# If a selection has been requested prepare a new message
# NOTE: The binary parts are available in the binParts list.
# newmsg is the newly construted message
	if ResourceName:
		newmsg = constructMessage(root, res, binParts)
		return newmsg
#        print newmsg.as_string()



if __name__ == '__main__':
	newmsg = main(sys.argv)
	print newmsg



# EOF